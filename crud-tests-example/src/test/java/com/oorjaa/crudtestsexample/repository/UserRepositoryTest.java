package com.oorjaa.crudtestsexample.repository;

import com.oorjaa.crudtestsexample.entities.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;

import java.util.List;

@SpringBootTest
class UserRepositoryTest {

    @Autowired
    UserRepository repository;

    @BeforeEach
    void setUp() {
    }

    @Test
    @Rollback
    void testAddGivenUser() {
        User user = new User(1, "Mandar", "Jalgaon");
        repository.addGivenUser(user);

        List<User> userList = repository.getAllUser();
        Assertions.assertEquals(user.getId(), userList.get(0).getId());
    }

    @Test
    @Rollback
    void testUpdateGivenUser() {
        User updatedUser = new User(1, "Mandar_Naik", "Jalgaon");
        repository.updateGivenUser(updatedUser);

        List<User> userList = repository.getAllUser();
        Assertions.assertEquals(updatedUser.getName(), userList.get(0).getName());
    }

    @Test
    @Rollback
    void testGetGivenUser() {
        List<User> userList = repository.getAllUser();
        User expectedUser = userList.get(0);

        User actualUser = repository.getGivenUser(1);

        Assertions.assertEquals(expectedUser.getId(), actualUser.getId());
    }

    @Test
    @Rollback
    void testDeleteGivenUser() {
        int listSizeBeforeDeletion = repository.getAllUser().size();
        int actualListSize = listSizeBeforeDeletion - 1;

        repository.deleteGivenUser(1);

        List<User> userList = repository.getAllUser();
        Assertions.assertEquals(actualListSize, userList.size());
    }
}