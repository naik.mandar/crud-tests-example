package com.oorjaa.crudtestsexample.controller;

import com.oorjaa.crudtestsexample.entities.User;
import com.oorjaa.crudtestsexample.services.UserService;
import org.junit.jupiter.api.*;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

@SpringBootTest
class ControllerTest {

    @Autowired
    private Controller controller;

    @MockBean
    private UserService userService;

    @BeforeEach
    void setUp() {
    }

    @Test
    void givenUser_whenAddNewUser_thenReturnHttpsStatus201() {
        User user = new User(1, "Mandar", "Jalgaon");
        Mockito.when(userService.addUser(user)).thenReturn(true);
        ResponseEntity<User> response = controller.addNewUser(user);
        Assertions.assertEquals(HttpStatus.CREATED, response.getStatusCode());
        Assertions.assertEquals(user.getId(), response.getBody().getId());
        Assertions.assertEquals(user.getName(), response.getBody().getName());
        Assertions.assertEquals(user.getAddress(), response.getBody().getAddress());
    }

    @Test
    void givenUser_whenAddNewUser_thenReturnHttpsStatus500() {
        User user = new User(1, "Mandar", "Jalgaon");
        Mockito.when(userService.addUser(user)).thenReturn(false);
        ResponseEntity<User> response = controller.addNewUser(user);
        Assertions.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
    }

    @Test
    void givenUserId_whenGetUser_thenReturnHttpsStatus404() {
        User user = new User(1, "Mandar Naik", "Jalgaon");
        long userId = user.getId();
        Mockito.when(userService.getUser(userId)).thenReturn(null);
        ResponseEntity<User> response = controller.getUserById(userId);
        Assertions.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    void givenUserId_whenGetUser_thenReturnHttpsStatus200() {
        User user = new User(1, "Mandar Naik", "Jalgaon");
        long userId = user.getId();
        Mockito.when(userService.getUser(userId)).thenReturn(user);
        ResponseEntity<User> response = controller.getUserById(userId);
        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assertions.assertEquals(user.getId(), response.getBody().getId());
        Assertions.assertEquals(user.getName(), response.getBody().getName());
        Assertions.assertEquals(user.getAddress(), response.getBody().getAddress());
    }

    @Test
    void givenUserId_whenUpdateUser_thenReturnHttpsStatus200() {
        User user = new User(1, "Mandar Naik", "Jalgaon");
        Mockito.when(userService.updateUser(user)).thenReturn(user);
        ResponseEntity<User> response = controller.updateExistingUser(user);
        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assertions.assertEquals(user.getId(), response.getBody().getId());
        Assertions.assertEquals(user.getName(), response.getBody().getName());
        Assertions.assertEquals(user.getAddress(), response.getBody().getAddress());
    }

    @Test
    void givenUserId_whenUpdateUser_thenReturnHttpsStatus404() {
        User user = new User(1, "Mandar Naik", "Jalgaon");
        Mockito.when(userService.updateUser(user)).thenReturn(null);
        ResponseEntity<User> response = controller.updateExistingUser(user);
        Assertions.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    void givenUserId_whendeleteUser_thenReturnHttpsStatus200() {
        User user = new User(1, "Mandar Naik", "Jalgaon");
        long userId = user.getId();
        Mockito.when(userService.deleteUser(userId)).thenReturn(true);
        ResponseEntity<User> response = controller.deleteUserById(userId);
        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    void givenUserId_whendeleteUser_thenReturnHttpsStatus404() {
        User user = new User(1, "Mandar Naik", "Jalgaon");
        long userId = user.getId();
        Mockito.when(userService.deleteUser(userId)).thenReturn(false);
        ResponseEntity<User> response = controller.deleteUserById(userId);
        Assertions.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void shouldHaveRestControllerAnnotation() {
        Annotation annotation = Controller.class.
                getAnnotation(RestController.class);
        if(annotation != null) {
            RestController restController = (RestController) annotation;
            Assertions.assertEquals(restController, Controller.class.getAnnotation(RestController.class));
        } else {
            Assertions.fail("Annotation or Required Parameter Not Present");
        }
    }

    @Test
    public void testAddUserMethodWithAnnotationAndParameters() throws SecurityException, NoSuchMethodException {
        Class<Controller> resourceClass = Controller.class;
        Method method = resourceClass.getMethod("addNewUser", User.class);
        Annotation annotation = method.getAnnotation(PostMapping.class);
        if(annotation instanceof PostMapping postMapping) {
            Assertions.assertEquals(postMapping, resourceClass.getMethod("addNewUser", User.class).getAnnotation(PostMapping.class));
        } else {
            Assertions.fail("Annotation or Required Parameter Not Present");
        }
    }

    @Test
    public void testUpdateUserInfoMethodWithAnnotationAndParameters() throws SecurityException, NoSuchMethodException {
        Class<Controller> resourceClass = Controller.class;
        Method method = resourceClass.getMethod("updateExistingUser", User.class);
        Annotation annotation = method.getAnnotation(PutMapping.class);
        if(annotation instanceof PutMapping putMapping) {
            Assertions.assertEquals(putMapping, resourceClass.getMethod("updateExistingUser", User.class).getAnnotation(PutMapping.class));
        } else {
            Assertions.fail("Annotation or Required Parameter Not Present");
        }
    }

    @Test
    public void testGetUserMethodWithAnnotationAndParameters() throws SecurityException, NoSuchMethodException {
        Class<Controller> resourceClass = Controller.class;
        Method method = resourceClass.getMethod("getUserById", long.class);
        Annotation annotation = method.getAnnotation(GetMapping.class);
        if(annotation instanceof GetMapping getMapping) {
            Assertions.assertEquals(getMapping, resourceClass.getMethod("getUserById", long.class).getAnnotation(GetMapping.class));
        } else {
            Assertions.fail("Annotation or Required Parameter Not Present");
        }
    }

    @Test
    public void testDeleteUserMethodWithAnnotationAndParameters() throws SecurityException, NoSuchMethodException {
        Class<Controller> resourceClass = Controller.class;
        Method method = resourceClass.getMethod("deleteUserById", long.class);
        Annotation annotation = method.getAnnotation(DeleteMapping.class);
        if(annotation instanceof DeleteMapping deleteMapping) {
            Assertions.assertEquals(deleteMapping, resourceClass.getMethod("deleteUserById", long.class).getAnnotation(DeleteMapping.class));
        } else {
            Assertions.fail("Annotation or Required Parameter Not Present");
        }
    }
}