package com.oorjaa.crudtestsexample.services;

import com.oorjaa.crudtestsexample.entities.User;
import com.oorjaa.crudtestsexample.repository.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@SpringBootTest
class UserServiceTest {

    @MockBean
    UserRepository repository;

    @BeforeEach
    void setUp() {
    }

    @Test
    void givenUser_whenAddGivenUser_ThenReturnTrue() {
        User user = new User(1, "Mandar", "Jalgaon");
        Mockito.when(repository.addGivenUser(user)).thenReturn(true);
        boolean responce = repository.addGivenUser(user);
        Assertions.assertTrue(responce);
    }

    @Test
    void givenUser_whenAddGivenUser_ThenReturnFalse() {
        User user = new User(1, "Mandar", "Jalgaon");
        Mockito.when(repository.addGivenUser(user)).thenReturn(false);
        boolean responce = repository.addGivenUser(user);
        Assertions.assertFalse(responce);
    }

    @Test
    void givenUser_whenGetGivenUser_ThenReturnUser() {
        User user = new User(1, "Mandar", "Jalgaon");
        long userId = user.getId();
        Mockito.when(repository.getGivenUser(userId)).thenReturn(user);
        User responce = repository.getGivenUser(userId);
        Assertions.assertEquals(user.getId(), responce.getId());
        Assertions.assertEquals(user.getName(), responce.getName());
        Assertions.assertEquals(user.getAddress(), responce.getAddress());
    }

    @Test
    void givenUserNotExist_whenGetGivenUser_ThenReturnNull() {
        long userId = 1;
        Mockito.when(repository.getGivenUser(userId)).thenReturn(null);
        User responce = repository.getGivenUser(userId);
        Assertions.assertNull(responce);
    }

    @Test
    void givenExistingUser_whenUpdateUser_thenReturnUpdatedUser() {
        User updateInfo = new User(1, "Mandar Naik", "Jalgaon");
        Mockito.when(repository.updateGivenUser(updateInfo)).thenReturn(updateInfo);
        User updatedInfo = repository.updateGivenUser(updateInfo);
        Assertions.assertEquals(updateInfo.getId(), updatedInfo.getId());
        Assertions.assertEquals(updateInfo.getName(), updatedInfo.getName());
        Assertions.assertEquals(updateInfo.getAddress(), updatedInfo.getAddress());
    }

    @Test
    void givenUserNotExist_whenUpdateUser_thenReturnNull() {
        User updateInfo = new User(1, "Mandar Naik", "Jalgaon");
        Mockito.when(repository.updateGivenUser(updateInfo)).thenReturn(null);
        User updatedInfo = repository.updateGivenUser(updateInfo);
        Assertions.assertNull(updatedInfo);
    }

    @Test
    void givenExistingUser_whenDeleteGivenUser_ThenReturnTrue() {
        User user = new User(1, "Mandar", "Jalgaon");
        long userId = user.getId();
        Mockito.when(repository.deleteGivenUser(userId)).thenReturn(true);
        boolean responce = repository.deleteGivenUser(userId);
        Assertions.assertTrue(responce);
    }

    @Test
    void givenUserNotExist_whenDeleteGivenUser_ThenReturnFalse() {
        long userId = 1;
        Mockito.when(repository.deleteGivenUser(userId)).thenReturn(false);
        boolean responce = repository.deleteGivenUser(userId);
        Assertions.assertFalse(responce);
    }
}