package com.oorjaa.crudtestsexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrudTestsExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrudTestsExampleApplication.class, args);
	}

}
