package com.oorjaa.crudtestsexample.repository;

import com.oorjaa.crudtestsexample.entities.User;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class UserRepository {
    private List<User> users = new ArrayList<>();

    public boolean addGivenUser(User user) {
        return users.add(user);
    }

    public User updateGivenUser(User user) {
        boolean listEmpty = users.isEmpty();
        if (!listEmpty) for (User currentUser : users) {
            if(currentUser.getId() == user.getId()) {
                return users.set(users.indexOf(currentUser), user);
            }
        }
        return null;
    }

    public  boolean deleteGivenUser(long id) {
        for (User user : users) {
            if(user.getId() == id) {
                return users.remove(user);
            }
        }
        return false;
    }

    public User getGivenUser(long id) {
        for (User user : users) {
            if (user.getId() == id) {
                return user;
            }
        }
        return null;
    }

    public List<User> getAllUser() {
        return users;
    }
}
