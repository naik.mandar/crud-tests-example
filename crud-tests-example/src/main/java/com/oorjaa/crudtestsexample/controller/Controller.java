package com.oorjaa.crudtestsexample.controller;

import com.oorjaa.crudtestsexample.entities.User;
import com.oorjaa.crudtestsexample.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class Controller {

    @Autowired
    UserService userService;

    @PostMapping(value = "/user")
    public ResponseEntity<User> addNewUser(User user) {
        if(userService.addUser(user)) {
            return ResponseEntity.status(HttpStatus.CREATED).body(user);
        } else {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping(value = "/user/{id}")
    public ResponseEntity<User> getUserById(@PathVariable("id") long id) {
        if(userService.getUser(id) !=  null) {
            return ResponseEntity.status(HttpStatus.OK).body(userService.getUser(id));
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @PutMapping(value = "/update")
    public ResponseEntity<User> updateExistingUser(@RequestBody User user) {
        if(userService.updateUser(user) !=  null) {
            return ResponseEntity.status(HttpStatus.OK).body(userService.updateUser(user));
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity<User> deleteUserById(@PathVariable("id") long id) {
        if(userService.deleteUser(id)) {
            return ResponseEntity.status(HttpStatus.OK).build();
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }
}
