package com.oorjaa.crudtestsexample.services;

import com.oorjaa.crudtestsexample.entities.User;

public interface UserServiceInterface {
    boolean addUser(User user);
}
