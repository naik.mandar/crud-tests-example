package com.oorjaa.crudtestsexample.services;

import com.oorjaa.crudtestsexample.entities.User;
import com.oorjaa.crudtestsexample.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService implements UserServiceInterface{

    @Autowired
    private UserRepository repository;

    public boolean addUser(User user) {
        return repository.addGivenUser(user);
    }

    public User getUser(long id) {
        return repository.getGivenUser(id);
    }

    public User updateUser(User user) {
        return repository.updateGivenUser(user);
    }

    public boolean deleteUser(long id) {
        return repository.deleteGivenUser(id);
    }
}
