package com.oorjaa.crudtestsexample.entities;

public class User {
    private long id;
    private String name;
    private String address;

    public User(long id, String name, String address) {
        this.id = id;
        this.name = name;
        this.address = address;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }
}
